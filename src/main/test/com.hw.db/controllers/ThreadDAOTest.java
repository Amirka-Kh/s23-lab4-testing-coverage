package com.hw.db.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.User;


public class ThreadDAOTest {

    @ParameterizedTest
    @MethodSource("treeSortList")
    void TestChange(Integer id, Integer limit, Integer since, Boolean desc, String sql_expected) {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO mockForum = new ThreadDAO(mockJdbc);

        ThreadDAO.treeSort(id, limit, since, desc);

        Mockito.verify(mockJdbc).query(Mockito.eq(sql_expected), Mockito.any(PostDAO.PostMapper.class), Mockito.anyVararg());
    }

    private static Stream<Arguments> treeSortList() {
        return Stream.of(
            Arguments.of(0, null, 2, true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;"),
            Arguments.of(0, 7, 3, false, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
            Arguments.of(0, 1, 0, true, "SELECT * FROM posts WHERE thread = ? AND branch < (SELECT branch FROM posts WHERE id = ?) ORDER BY branch DESC  LIMIT ? ;")         
        );
    }
}

