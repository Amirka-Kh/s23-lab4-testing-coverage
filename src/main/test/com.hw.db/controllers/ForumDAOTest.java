package com.hw.db.DAO;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ForumDAOTest {

    @ParameterizedTest
    @MethodSource("userList")
    void TestUserList(String slug, Integer limit, String since, Boolean desc, String expected) {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO mockForum = new ForumDAO(mockJdbc);

        ForumDAO.UserList(slug, limit, since, desc);
        Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    private static Stream<Arguments> userList() {
        return Stream.of(
            Arguments.of("slug", null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
            Arguments.of("slug", 1, "10.01.2001", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Arguments.of("slug", 5, "10.12.2022", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
            Arguments.of("slug", null, "10.01.2001", null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
            Arguments.of("slug", 1, null, true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Arguments.of("slug", null, "02.02.2020", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
        );
    }


}
