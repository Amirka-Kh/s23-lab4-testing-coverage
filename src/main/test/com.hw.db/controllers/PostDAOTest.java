package com.hw.db.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.Post;

public class PostDAOTest {

    @ParameterizedTest
    @MethodSource("setPostList")
    void TestSetPost(Post post, String sql_expected) {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO mockPost = new PostDAO(mockJdbc);
        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0)
            )
        ).thenReturn(makePost("a", "m", new Timestamp(0)));

        PostDAO.setPost(0, post);
        if (sql_expected == null) {
            Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
        } else {
            Mockito.verify(mockJdbc).update(Mockito.eq(sql_expected), Optional.ofNullable(Mockito.anyVararg()));
        }
    }

    static Post makePost(String author, String message, Timestamp creationTime, String forum) {
        return new Post(author, creationTime, forum, message, null, null, null);
    }

    private static Stream<Arguments> setPostList() {
        return Stream.of(
            Arguments.of(makePost("a", "m", new Timestamp(0), null), null),
            Arguments.of(makePost("a", "m", new Timestamp(1), null), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("a", "mm", new Timestamp(0), null), "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("a", "mm", new Timestamp(1), null), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("aa", "m", new Timestamp(0), null), "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("aa", "m", new Timestamp(1), null), "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("aa", "mm", new Timestamp(0), null), "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(makePost("aa", "mm", new Timestamp(1), null), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }
}

