package com.hw.db.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.User;


public class UserDAOTest {

    @ParameterizedTest
    @MethodSource("changeList")
    void TestChange(User user, String sql_expected, Object[] params_expected) {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO mockForum = new UserDAO(mockJdbc);

        UserDAO.Change(user);

        if (sql_expected == null) {
            Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
        } else {
            assertEquals(params_expected.length, 2);
            Mockito.verify(mockJdbc).update(Mockito.eq(sql_expected), Mockito.eq(params_expected[0]), Mockito.eq(params_expected[1]));
        }
    }    

    private static Stream<Arguments> changeList() {
        return Stream.of(
            Arguments.of(new User("nick", null, null, null), null, null),
            Arguments.of(new User("nick", "email", null, null), "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;", new Object[]{"email", "nick"}),
            Arguments.of(new User("nick", null, "fullname", null), "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;", new Object[]{"fullname", "nick"}),
            Arguments.of(new User("nick", null, null, "about"), "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;", new Object[]{"about", "nick"}),
            Arguments.of(new User("nickname", null, null, "about"), "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;", new Object[]{"about", "nickname"})
        );
    }
}

